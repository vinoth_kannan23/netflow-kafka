
require 'bindata'

class Netflow5Packet < BinData::Record
   endian :big
   uint16 :version
   uint16 :flow_records
   uint32 :uptime
   uint32 :unix_sec
   uint32 :unix_nsec
   uint32 :flow_seq_num
   uint8  :engine_type
   uint8  :engine_id
   bit1   :sampling_type
   bit14  :sampling_interval

   array  :records, :initial_length => :flow_records do
      uint32 :srcaddr
      uint32 :dstaddr
      uint32 :nexthop
      uint16 :iface_in
      uint16 :iface_out
      uint32 :packets
      uint32 :octets
      uint32 :first_uptime
      uint32 :last_uptime
      uint16 :srcport
      uint16 :dstport
      uint8  :pad1
      uint8  :tcpflags
      uint8  :proto
      uint8  :tos
      uint16 :srcas
      uint16 :dstas
      uint8  :srcmask
      uint8  :dstmask
      uint16 :pad2
   end

end
